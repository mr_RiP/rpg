#-------------------------------------------------
#
# Project created by QtCreator 2016-02-26T00:46:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Game
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mainscreen.cpp

HEADERS  += mainwindow.h \
    mainscreen.h

FORMS    += mainwindow.ui \
    mainscreen.ui
